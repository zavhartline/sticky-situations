<img src="https://cdn.discordapp.com/attachments/894831460857556992/901081772312395786/ezgif-4-ae21047442a8.gif" width=400>

# Sticky Situations

A coordinate parenting manager for Create Your Frisk. This library lets the programmer define a PointController which lets them apply transformations to all child objects. Basically, if it has an `x`, `y`, and optionally a `rotation` member, it can be managed by a PointController. 

## Getting Started

**1.)** Place the StickySituations.lua in the Lua/Libraries folder. 

**2.)**  At the top of the file you want to use the library with, write
```lua
require 'Libraries/StickySituations'
``` 

Simple as that!

---

## Basic Usage Instructions:

There are a few key concepts to understand before using this library. First and foremost, a new instance of a PointController is created by running `PointController.New()`. When the PointController is moved, rotated, or scaled, all of its children will be scaled. To add a child to PointController, invoke any of the `pointcontrollerinstance:Register()` methods. Similarly, to remove a child use the unregister equivalents. This library will work with *any* tables/userdata that has writable x and y fields, meaning you could even make a PointController a child to another PointController!

**One very important note** is that scaling is done multiplicatively, based on the *current* distance from the origin, rather than the unscaled distance. This means scaling will be exponential as it's applied. 

To access the library's API, check out the [wiki page](https://gitlab.com/zavhartline/sticky-situations/-/wikis/API) for more details.

Interested in the example showcased above? The wave code is provided in the repository!

---
