PointController = {}

function PointController.New(x,y)
	PointController._pcheck("X", x, "number", "nil")
	PointController._pcheck("Y", y, "number", "nil")
	local mt = {rotation = 0,
				originX = x or 0,
				originY = y or 0,
				xscale = 1,
				yscale = 1,
				members = {},
				data = {},
				signature = 'pointcontroller'
			}
	mt['__index'] = function(t,k)
		if k == 'x' then return mt.originX end
		if k == 'y' then return mt.originY end
		if mt.data[k] == nil then 
			return PointController[k]
		end
		return mt.data[k]
	end
	mt['__newindex'] = function(t,k,v)
		if k == 'x' then 
			t:MoveTo(v,t.y)
			return 
		end
		if k == 'y' then 
			t:MoveTo(t.x,v)
			return 
		end
		mt.data[k] = v
	end
	return setmetatable({},mt)
end

function PointController:Register(val)
	PointController._pcheck("Registrant", val, "table", "userdata")
	getmetatable(self).members[val] = val
end

function PointController:RegisterAll(...)
	if #{...} == 1 then
		for i,v in ipairs(({...})[1]) do
			PointController._pcheck("Registrant", v, "table", "userdata")
			self:Register(v)
		end
		return
	end
	for i,v in ipairs({...}) do
		PointController._pcheck("Registrant", v, "table", "userdata")
		self:Register(v)
	end
end

function PointController:Unregister(val)
	getmetatable(self).members[val] = nil
end

function PointController:UnregisterAll(...)
	if #{...} == 1 then
		for i,v in ipairs(({...})[1]) do
			self:Unregister(v)
		end
		return
	end
	for i,v in ipairs({...}) do
		self:Unregister(v)
	end
end

function PointController:MovePolarMember(member,dr,dt)
	PointController._pcheck("Member", member, "table", "userdata")
	PointController._pcheck("Radius", dr, "number")
	PointController._pcheck("Degrees", dt, "number")
	if getmetatable(self).members[member] == nil then
		error("Nonexistent member of PointController", 2)
	end
	local tuple = PointController._rotateabout(math.rad(dt),member.x,member.y,self:GetX(),self:GetY())
	member.x = tuple.x
	member.y = tuple.y
	local newrot = math.rad(self:GetRotation() + dt)
	local dx, dy = self:GetX()-member.x, self:GetY()-member.y
	member.x = member.x - math.cos(math.atan2(dy,dx)) * dr
	member.y = member.y - math.sin(math.atan2(dy,dx)) * dr
end

function PointController:MoveCartesianMemberTo(member,x,y)
	PointController._pcheck("Member", member, "table", "userdata")
	PointController._pcheck("X", x, "number")
	PointController._pcheck("Y", y, "number")
	if getmetatable(self).members[member] == nil then
		error("Nonexistent member of PointController", 2)
	end
  	local tuple = PointController._rotateabout(math.rad(self:GetRotation()),x+self:GetX(),y+self:GetY(),self:GetX(),self:GetY())
  	member.x = tuple.x
  	member.y = tuple.y
end

function PointController:MoveCartesianMember(member,dx,dy)
	PointController._pcheck("Member", member, "table", "userdata")
	PointController._pcheck("X", dx, "number")
	PointController._pcheck("Y", dy, "number")
	if getmetatable(self).members[member] == nil then
		error("Nonexistent member of PointController", 2)
	end

  	local tuple = PointController._rotateabout(math.rad(-self:GetRotation()),member.x,member.y,self:GetX(),self:GetY())
  	tmpx = tuple.x + dx
  	tmpy = tuple.y + dy
  	tuple = PointController._rotateabout(math.rad(self:GetRotation()),tmpx,tmpy,self:GetX(),self:GetY())
  	member.x = tuple.x
  	member.y = tuple.y
end

function PointController:MoveTo(x,y)
	PointController._pcheck("X", x, "number")
	PointController._pcheck("Y", y, "number")
	local dx = x - getmetatable(self).originX
	local dy = y - getmetatable(self).originY
	for _,v in pairs(getmetatable(self).members) do
		if x ~= nil then v.x = v.x + dx end
		if y ~= nil then v.y = v.y + dy end
	end
	getmetatable(self).originX = x
	getmetatable(self).originY = y
end

function PointController:Move(x,y)
	PointController._pcheck("X", x, "number")
	PointController._pcheck("Y", y, "number")
	for _,v in pairs(getmetatable(self).members) do
		if x ~= nil then v.x = v.x + x end
		if y ~= nil then v.y = v.y + y end
	end
	getmetatable(self).originX = getmetatable(self).originX + x
	getmetatable(self).originY = getmetatable(self).originY + y
end

function PointController:Rotate(deg, inherit)
	PointController._pcheck("Degrees", deg, "number")
	PointController._pcheck("Degrees", inherit, "boolean", "nil")
  	for _,v in pairs(getmetatable(self).members) do
  		local tuple = PointController._rotateabout(math.rad(deg),v.x,v.y,self:GetX(),self:GetY())
  		v.x = tuple.x
  		v.y = tuple.y
  		if inherit == true then
  			if pcall(function() v.rotation = v.rotation+deg end) then
  			elseif pcall(function() v.sprite.rotation = v.sprite.rotation+deg end) then
  			elseif getmetatable(v).signature == 'pointcontroller' then
  				v:Rotate(deg, inherit)
  			end
  		end
  	end
  	getmetatable(self).rotation = (getmetatable(self).rotation + deg) % 360
end

function PointController:Scale(xf, yf)
	PointController._pcheck("XScale", xf, "number")
	PointController._pcheck("YScale", yf, "number", "nil")
	if yf == nil then yf = xf end
  	for _,v in pairs(getmetatable(self).members) do
  		if xf ~= nil then
  			v.x = (v.x-getmetatable(self).originX) * xf + getmetatable(self).originX
  		end
  		if yf ~= nil then
  			v.y = (v.y-getmetatable(self).originY) * yf + getmetatable(self).originY
  		end
  	end
  	getmetatable(self).xscale = getmetatable(self).xscale * xf
  	getmetatable(self).yscale = getmetatable(self).yscale * yf
end

function PointController:GetRotation()
	return getmetatable(self).rotation
end
function PointController:GetMembers()
	return getmetatable(self).members
end
function PointController:GetXScale()
	return getmetatable(self).xscale
end
function PointController:GetYScale()
	return getmetatable(self).yscale
end
function PointController:GetX()
	return getmetatable(self).originX
end
function PointController:GetY()
	return getmetatable(self).originY
end

function PointController._rotateabout(rad,px,py,ox,oy)
	local sin = math.sin(rad)
	local cos = math.cos(rad)
	local dx, dy = px - ox, py - oy
	return {x=cos*dx - sin*dy + ox, y=sin*dx + cos*dy + oy}
end

function PointController._pcheck(title,obj,...)
	local errstr = title .. " expected type(s) "
	for i,v in ipairs({...}) do
		if type(obj) == v then return end
		if i == 1 then 
			errstr = errstr .. v
		else errstr = errstr .. " or " .. v
		end
	end
	error(errstr..", found " .. type(obj), 3)
end
return PointController
