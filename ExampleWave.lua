require "StickySituations"

center = PointController.New(Player.x, Player.y)
rightAnchor = PointController.New(Player.x + 100, Player.y)
leftAnchor = PointController.New(Player.x - 100, Player.y)
topAnchor = PointController.New(Player.x + 50, Player.y)
bottomAnchor = PointController.New(Player.x - 50, Player.y)
center:RegisterAll(rightAnchor,leftAnchor,topAnchor,bottomAnchor, {x=5,y=2})
--Generate the long tendril thingies
for i=0, 10 do
	rightAnchor:Register(CreateProjectile('bullet', rightAnchor.x + 10 * i, rightAnchor.y + 20*math.sqrt(i)))
	leftAnchor:Register(CreateProjectile('bullet', leftAnchor.x - 10 * i, leftAnchor.y - 20*math.sqrt(i)))
end
--Generate + shape out of boolet
pattern = {{-1,0},{0,-1},{1,0},{0,1}}
for i=1,4 do
	topAnchor:Register(CreateProjectile('bullet', topAnchor.x + 25 * pattern[i][1], topAnchor.y + 25 * pattern[i][2]))
	bottomAnchor:Register(CreateProjectile('bullet', bottomAnchor.x + 25 * pattern[i][1], bottomAnchor.y + 25 * pattern[i][2]))
end

function Update()
    --Rotates by T degrees each frame
    rightAnchor:Rotate(2)
   	leftAnchor:Rotate(2)
   	topAnchor:Rotate(5)
   	bottomAnchor:Rotate(5)
    center:Rotate(0.5)
end
