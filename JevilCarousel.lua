require "StickySituations"

function ellipse(a)
	return {x=-120 * math.sin(math.rad(a-10)), y=10 * math.cos(math.rad(a-10))}
end

center = PointController.New(Player.x, Player.y+100)
centermarker = CreateProjectile('bullet', center.x, center.y)
centermarker.sprite.color32 = {255,0,0}
center:Register(centermarker)
columnList = {}
local colcount = 8
for i=0, colcount-1 do
	local column = PointController.New(center.x + ellipse(i*360/colcount).x, center.y + ellipse(i*360/colcount).y - 20)
	local heightController = PointController.New(column.x, column.y)
	
	heightController:Register(CreateProjectile('bullet', column.x, column.y - 40))
	heightController:Register(CreateProjectile('bullet', column.x, column.y - 90))
	heightController:Register(CreateProjectile('bullet', column.x, column.y - 140))

	for _,v in pairs(heightController:GetMembers()) do
		v.sprite.layer = 'BelowArena'
	end

	column:Register(heightController)
	table.insert(columnList, column)
	column.heightController = heightController
	center:Register(column)
end
t = 0

function OnHit(boolet)
	if boolet.sprite.layer ~= 'BelowArena' then Player.Hurt(3) end
end

function Update()
	local tilt = .75
	centermarker.sprite.rotation = center:GetRotation()
	if (t+75) % 300 > 150 and t % 2 == 0then
		center:Rotate(tilt)
	elseif t % 2 == 0 then
		center:Rotate(-tilt)
	end
	
	for i=1, #columnList do
		local mult = -1
		if (i-1) % 2 == 0 then mult = 1 end
		--Move around
		center:MoveCartesianMemberTo(columnList[i], ellipse(t + (i*360/colcount)).x, ellipse(t + (i*360/colcount)).y)
		--Move up/down
		columnList[i]:MoveCartesianMember(columnList[i].heightController, 0, mult*math.sin(math.rad(180 + t*2))/2)

		local threshold = 100
		local scalecoeff = 0.05
		if columnList[i].x > Arena.width/2 then
			for _,v in pairs(columnList[i].heightController:GetMembers()) do
				if v.sprite.xscale < 0 then
					v.sprite.color32 = {128,128,128}
					v.sprite.layer = 'BelowArena'
				end
				if v.x > threshold and v.sprite.xscale > -1 then
					v.sprite.xscale = v.sprite.xscale - scalecoeff
				end
			end
		end
		if columnList[i].x < -Arena.width/2 then
			for _,v in pairs(columnList[i].heightController:GetMembers()) do
				if v.sprite.xscale > 0 then
					v.sprite.layer = 'Top'
					v.sprite.color32 = {255,255,255}
				end
				if v.x < -threshold and v.sprite.xscale < 1 then
					v.sprite.xscale = v.sprite.xscale + scalecoeff
				end
			end
		end

	end
	t = t + 1
end
